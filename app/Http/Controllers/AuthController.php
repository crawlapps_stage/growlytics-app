<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Osiset\ShopifyApp\Actions\AuthenticateShop;
use Osiset\ShopifyApp\Exceptions\MissingAuthUrlException;
use Osiset\ShopifyApp\Exceptions\SignatureVerificationException;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use Osiset\ShopifyApp\Util;

class AuthController extends Controller
{
    public function authenticate(Request $request, AuthenticateShop $authShop)
    {
        // Get the shop domain
        $shopDomain = $request->has('shop')
            ? ShopDomain::fromNative($request->get('shop'))
            : $request->user()->getDomain();

        // If the domain is obtained from $request->user()
        if ($request->missing('shop')) {
            $request['shop'] = $shopDomain->toNative();
        }

        // Run the action
        [$result, $status] = $authShop($request);

//        generate access_token
//        if($result['completed']){
//            $this->createAccessToken($request);
//        }

        if ($status === null) {
            // Show exception, something is wrong
            throw new SignatureVerificationException('Invalid HMAC verification');
        } elseif ($status === false) {
            if (! $result['url']) {
                throw new MissingAuthUrlException('Missing auth url');
            }

            return View::make(
                'shopify-app::auth.fullpage_redirect',
                [
                    'authUrl' => $result['url'],
                    'shopDomain' => $shopDomain->toNative(),
                ]
            );
        } else {
            // Go to home route
            return Redirect::route(
                Util::getShopifyConfig('route_names.home'),
                [
                    'shop' => $shopDomain->toNative(),
                    'host' => $request->host,
                ]
            );
        }
    }

    public function createAccessToken($request){
        try{
//            $parameter =
        }catch(\Exception $e){
            logger($e);
        }
    }
}
