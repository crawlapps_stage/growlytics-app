<?php

namespace App\Http\Controllers\BeforeInstall;

use App\Http\Controllers\Controller;
use App\Models\GrowlyticsDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class BeforeInstallController extends Controller
{
    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        try{
            $user = Auth::user();
            $data['shop']['name'] = $user->name;
            $data['shop']['is_synced'] = $user->is_synced;
            return response()->json(['data' => $data], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function connectAccount(Request $request){
        try{
            $data = $request->data;
            $key = $data['key'];
            $user = Auth::user();

            $endPoint = config('const.connect_host').'/connector-app/auth';
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($endPoint, [
                "apikey" => $key,
                "shop" => $user->name,
            ]);

            $result = ($response->successful()) ? json_decode($response->getBody()) : [];

            return response()->json(['data' => $result], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function syncAccount(Request $request){
        try{
            $user = Auth::user();
            $data =  $request->data;

            $endPoint = config('const.connect_host').'/connector-app/sync';
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($endPoint, [
                "apikey" => $data['apikey'],
                "accessToken" => $user->password,
                "shop" => $user->name,
            ]);

            $failedRes = [
                'success' =>  false,
                'message' => 'Sync failed',
            ];

            $res = ($response->successful()) ? json_decode($response->getBody()) : $failedRes;

            if($res->success){
                $this->saveGrowlytics($data, $user);
                $user->is_synced = true;
                $user->save();
            }
            return response()->json(['data' => $res], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function syncStatus(Request $request){
        try{
            $user = Auth::user();
            $growlytics =  GrowlyticsDetail::select('login_data')->where('user_id', $user->id)->orderBy('created_at', 'desc')->first();

            $data = json_decode($growlytics->login_data);

            $endPoint = config('const.connect_host').'/connector-app/status';
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->post($endPoint, [
                "apikey" => $data->apikey,
                "shop" => $user->name,
            ]);

            $res = ($response->successful()) ? json_decode($response->getBody()) : [];

            if($res->success){
                $data = [
                    "success"=> true,
                        "data"=> [
                            "name"=> "I Say Organic",
                            "syncing"=> true,
                            "products"=> 489,
                            "orders"=> 5444,
                            "customers"=> 3533
                        ]
                ];
                return response()->json(['data' => $data], 200);
            }
            return response()->json(['data' => $res], 200);
        }catch(\Exception $e){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function saveGrowlytics($data, $user){
        try{
            $growlytics = GrowlyticsDetail::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
            $growlyticsData = ($growlytics) ? $growlytics : new GrowlyticsDetail;
            $growlyticsData->user_id = $user->id;
            $growlyticsData->login_data = json_encode($data);
            $growlyticsData->save();
            return true;
        }catch(\Exception $e){
            logger($e);
            return false;
        }
    }
}
