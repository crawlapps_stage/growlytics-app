# Shopify Growlytics App

### Installation
Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret, mail credentials)
$ php artisan migrate
```

For development environments...

```sh
$ npm install
$ npm run dev
```
For production environments...

```sh
$ npm install --production
$ npm run prod
```

Extra commands

```sh
$ php artisan db:seed
```
### Used Shopify Tools

* Admin rest-api
* Shopify GraphQL api
* App-bridge
