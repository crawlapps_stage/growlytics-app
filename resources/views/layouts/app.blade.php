<html>
<head class="growlytics-app-main">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Growlytics App') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- style custom css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Polaris css cdn -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.1.0/dist/styles.css" type="text/css" />

    <!-- font-awesome link -->
    <script src="https://kit.fontawesome.com/7945acc650.js" crossorigin="anonymous"></script>

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge@1.28.0"></script>
        <script>
            var AppBridge = window['app-bridge'];

            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
</head>
<body class="">

<div id="app">
    <router-view></router-view>
</div>
</body>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
</html>

