import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/BeforeSync/BeforeInstall').default,
        name:'before-installing',
        meta: {
            title: 'Before Installing',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
            isBeforeInstall: true,
            isClickable: true
        },
    },
    {
        path:'/connect/:shopname',
        component: require('../components/pages/BeforeSync/Connect').default,
        name:'connect',
        meta: {
            title: 'Connect',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
            isBeforeInstall: true,
            isClickable: false
        },
    },
    {
        path:'/sync',
        component: require('../components/pages/BeforeSync/Sync').default,
        name:'sync',
        meta: {
            title: 'Sync',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
            isBeforeInstall: true,
            isClickable: false
        },
    },
    {
        path:'/status',
        component: require('../components/pages/AfterSync/Status').default,
        name:'status',
        meta: {
            title: 'Status',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
            isBeforeInstall: false,
            isClickable: true
        },
    },
];
// This callback runs before every route change, including on page load.
const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});
export default router;
