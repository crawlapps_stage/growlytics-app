<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BeforeInstall\BeforeInstallController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('layouts.app');
})->middleware(['verify.shopify'])->name('home');

Route::group(['middleware' => 'verify.shopify'], function () {
    Route::get('get-logindata', [BeforeInstallController::class, 'index'])->name('get-logindata');
    Route::post('connect-account', [BeforeInstallController::class, 'connectAccount'])->name('connect-account');
    Route::post('sync-account', [BeforeInstallController::class, 'syncAccount'])->name('sync-account');
    Route::get('sync-status', [BeforeInstallController::class, 'syncStatus'])->name('sync-status');
});

//flush session
Route::get('flush', function(){
    request()->session()->flush();
});
